import { REQUEST_URL } from '../utils/env';
import { get } from './Fetch';
import { ICharactersResponse } from '../interfaces/responses/ICharacterResponse';
import { IComicResponse } from '../interfaces/responses/IComicResponse';
import { IStoryResponse } from '../interfaces/responses/IStoryResponse';

export const fetchCharacters = async () => {
  const response = await get<ICharactersResponse>(REQUEST_URL('characters')).then((r) => r.data.results.map((char) => {
    return {
      id: char.id,
      name: char.name,
      img: `${char.thumbnail.path}.${char.thumbnail.extension}`,
    };
  }));
  return response;
};

export const fetchComics = async () => {
  const response = await get<IComicResponse>(REQUEST_URL('comics')).then((r) => r.data.results.map((com) => {
    return {
      id: com.id,
      name: com.title,
      img: `${com.thumbnail.path}.${com.thumbnail.extension}`,
    };
  }));
  return response;
};

export const fetchStories = async () => {
  const response = await get<IStoryResponse>(REQUEST_URL('stories')).then((r) => r.data.results.map((story) => {
    return {
      id: story.id,
      name: story.title,
    };
  }));
  return response;
};
