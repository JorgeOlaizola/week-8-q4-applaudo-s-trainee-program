import { Dispatch } from 'redux';
import TStoryCard from '../../interfaces/card/TStoryCard';
import { stories } from './actionsTypes';

export const dispatchStories = (payload: TStoryCard[]) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: stories.SET_STORIES,
      payload,
    });
  };
};

export const searchStories = (payload: TStoryCard[]) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: stories.SEARCH,
      payload,
    });
  };
};
