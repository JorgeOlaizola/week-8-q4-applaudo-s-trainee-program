import { Dispatch } from 'redux';
import { bookmarks } from './actionsTypes';

export const addBookmark = (type: string, payload: any) => {
  return (dispatch: Dispatch) => {
    switch (type) {
      case 'characters':
        return dispatch({ type: bookmarks.ADD_BOOKMARK_CHARACTER, payload });
      case 'comics':
        return dispatch({ type: bookmarks.ADD_BOOKMARK_COMIC, payload });
      case 'stories':
        return dispatch({ type: bookmarks.ADD_BOOKMARK_STORY, payload });
      default:
        return null;
    }
  };
};

export const removeBookmark = (type: string, id: number) => {
  return (dispatch: Dispatch) => {
    switch (type) {
      case 'characters':
        return dispatch({ type: bookmarks.REMOVE_BOOKMARK_CHARACTER, payload: id });
      case 'comics':
        return dispatch({ type: bookmarks.REMOVE_BOOKMARK_COMIC, payload: id });
      case 'stories':
        return dispatch({ type: bookmarks.REMOVE_BOOKMARK_STORY, payload: id });
      default:
        return null;
    }
  };
};

export const clearBookmarks = () => {
  return (dispatch: Dispatch) => {
    dispatch({ type: bookmarks.CLEAR_BOOKMARKS });
  };
};
