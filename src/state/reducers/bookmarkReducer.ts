import { bookmarks } from '../actions/actionsTypes';

type TItem = {
  name?: string;
  title?: string;
  img?: string;
  id: number;
  description: string | null;
};

type TInitialState = {
  characters: TItem[];
  stories: TItem[];
  comics: TItem[];
};
const initialState: TInitialState = {
  characters: [],
  stories: [],
  comics: [],
};

const bookmarksReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case bookmarks.ADD_BOOKMARK_CHARACTER:
      return { ...state, characters: [...state.characters, action.payload] };
    case bookmarks.ADD_BOOKMARK_COMIC:
      return { ...state, comics: [...state.comics, action.payload] };
    case bookmarks.ADD_BOOKMARK_STORY:
      return { ...state, stories: [...state.stories, action.payload] };
    case bookmarks.REMOVE_BOOKMARK_CHARACTER:
      return {
        ...state,
        characters: [...state.characters.filter((char) => char.id !== action.payload)],
      };
    case bookmarks.REMOVE_BOOKMARK_COMIC:
      return { ...state, comics: [...state.comics.filter((comic) => comic.id !== action.payload)] };
    case bookmarks.REMOVE_BOOKMARK_STORY:
      return {
        ...state,
        stories: [...state.stories.filter((story) => story.id !== action.payload)],
      };
    case bookmarks.CLEAR_BOOKMARKS:
      return initialState;
    default:
      return state;
  }
};

export default bookmarksReducer;
