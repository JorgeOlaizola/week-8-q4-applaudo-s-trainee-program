import { characters } from '../actions/actionsTypes';

const initialState = {
  characters: [],
  charactersCopy: []
};

const charactersReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case characters.SET_CHARACTERS:
      return { ...state, characters: action.payload, charactersCopy: action.payload };
    case characters.SEARCH:
      return { ...state, characters: action.payload };
    default:
      return state;
  }
};

export default charactersReducer;
