import { stories } from '../actions/actionsTypes';

const initialState = {
  stories: [],
  storiesCopy: []
};

const storiesReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case stories.SET_STORIES:
      return { ...state, stories: action.payload, storiesCopy: action.payload };
    case stories.SEARCH:
      return { ...state, stories: action.payload };
    default:
      return state;
  }
};

export default storiesReducer;
