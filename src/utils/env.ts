export const BASE_URL = 'https://gateway.marvel.com/v1/public/';
export const PRIVATE_KEY = '2a2f18789d2af354f5d5924d723198763cdc2e1d';
export const PUBLIC_KEY = '8136fd0bfbd8160bc084723dd4043c98';
export const HASH_KEY = 'b81c8bc7221c2b43ebe6622401db5b79';
export const REQUEST_URL = (path: string) => `${BASE_URL}/${path}?apikey=${PUBLIC_KEY}&hash=${HASH_KEY}&ts=1&limit=40`;
