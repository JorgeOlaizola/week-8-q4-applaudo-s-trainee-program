import { ICharacterDetail } from '../detail/ICharacterDetail';
import { IComicDetail } from '../detail/IComicDetail';
import { IStoryDetail } from '../detail/IStoryDetail';

type IDetailResponse = IComicDetail & ICharacterDetail & IStoryDetail;

export default IDetailResponse;
