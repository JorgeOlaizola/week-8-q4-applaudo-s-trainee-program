type TStoryCard = {
    name: string,
    id: number,
}

export default TStoryCard;
