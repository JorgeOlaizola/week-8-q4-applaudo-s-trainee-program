import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../state/reducers';
import './searchbar.scss';
import { searchCharacters } from '../../../state/actions/charactersActions';
import TCard from '../../../interfaces/card/TCard';

export default function index() {
  const state = useSelector((state: State) => state.characters.charactersCopy);
  const dispatch = useDispatch();

  const changeHandler = (input: string) => {
    dispatch(
      searchCharacters(
        state.filter((item: TCard) => item.name.toLowerCase().includes(input.toLowerCase()))
      )
    );
  };
  return (
    <div className='searchbar-container'>
      <input
        className='searchbar-input'
        onChange={(e) => changeHandler(e.target.value)}
        type='text'
        placeholder='Search'
      />
    </div>
  );
}
