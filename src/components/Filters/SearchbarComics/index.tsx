import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../state/reducers';
import '../SearchbarCharacters/searchbar.scss';
import TCard from '../../../interfaces/card/TCard';
import { searchComics } from '../../../state/actions/comicsActions';

export default function index() {
  const state = useSelector((state: State) => state.comics.comicsCopy);
  const dispatch = useDispatch();

  const changeHandler = (input: string) => {
    dispatch(
      searchComics(
        state.filter((item: TCard) => item.name.toLowerCase().includes(input.toLowerCase()))
      )
    );
  };
  return (
    <div className='searchbar-container'>
      <input
        className='searchbar-input'
        onChange={(e) => changeHandler(e.target.value)}
        type='text'
        placeholder='Search'
      />
    </div>
  );
}
