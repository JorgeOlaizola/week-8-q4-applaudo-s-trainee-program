import React from 'react';
import './cardContainer.scss';
import Card from '../CardItem';
import TCard from '../../../interfaces/card/TCard';
import TDetail from '../../../interfaces/detail/TDetail';

export default function index({ items, type }: { items: TCard[], type: TDetail }) {
  return <div className='characters-container'>{items && items.map(({ name, id, img }: TCard) => (<Card name={name} id={id} key={id} img={img} type={type} />))}</div>;
}
