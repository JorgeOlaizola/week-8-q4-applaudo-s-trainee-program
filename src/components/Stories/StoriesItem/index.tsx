import React from 'react';
import { useNavigate } from 'react-router-dom';
import './storiesItem.scss';
import TStoryCard from '../../../interfaces/card/TStoryCard';

export default function index({ name, id }: TStoryCard) {
  const navigate = useNavigate();
  return (
    <div onClick={() => navigate(`/stories/${id}`)} className='story-container'>
      {name}
      <span className='story-detail'>Click here to see more details 🖱</span>
    </div>
  );
}
