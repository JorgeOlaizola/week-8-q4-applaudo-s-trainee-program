import React from 'react';
import TStoryCard from '../../../interfaces/card/TStoryCard';
import StoriesItem from '../StoriesItem';
import './storiesContainer.scss';

export default function index({ stories }: { stories: TStoryCard[] }) {
  return (
    <div className='stories-container'>
      {stories &&
        stories.map((story: TStoryCard) => (
          <StoriesItem name={story.name} id={story.id} key={story.id} />
        ))}
    </div>
  );
}
