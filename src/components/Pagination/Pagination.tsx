import React, { useMemo } from 'react';
import './pagination.scss';

type TPagination<T> = {
  itemsPerPage: T;
  totalItems: T;
  paginate: Function;
  currentPage: T;
  noScroll: boolean;
};

export default function Pagination<T extends number>({
  itemsPerPage,
  totalItems,
  paginate,
  currentPage,
  noScroll,
}: TPagination<T>) {
  const pageNumbers = [];
  const totalPages = useMemo(
    () => Math.ceil(totalItems / itemsPerPage),
    [itemsPerPage, totalItems]
  );

  for (let i = 1; i <= totalPages; i++) {
    pageNumbers.push(i);
  }

  const clickFunction = (page: number) => {
    paginate(page);
    if (!noScroll) window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  };

  return (
    <div className='pagination-container'>
      {currentPage !== 1 && totalItems > 0 && (
        <button
          type='button'
          className='pagination-item'
          onClick={() => clickFunction(currentPage - 1)}
        >
          ⬅
        </button>
      )}
      {pageNumbers.map((page) => (
        <button
          type='button'
          key={page}
          className={currentPage === page ? 'pagination-item-selected' : 'pagination-item'}
          onClick={() => clickFunction(page)}
        >
          {page}
        </button>
      ))}
      {currentPage !== pageNumbers[pageNumbers.length - 1] && totalItems > 0 && (
        <button
          type='button'
          className='pagination-item'
          onClick={() => clickFunction(currentPage + 1)}
        >
          ➡
        </button>
      )}
    </div>
  );
}
