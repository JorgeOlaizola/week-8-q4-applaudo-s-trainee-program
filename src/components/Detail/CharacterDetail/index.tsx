import React, { useEffect, useState } from 'react';
import './characterDetail.scss';
import '../detailStyles.scss';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { addBookmark, removeBookmark } from '../../../state/actions/bookmarksActions';
import { State } from '../../../state/reducers';

interface IStories {
  name: string;
  resourceURI: string;
  type: string;
}

interface IComics {
  name: string;
  resourceURI: string;
}

interface ICharacterDetail {
  description: string | null;
  id: number;
  name: string;
  img: string;
  source: string;
  stories: IStories[] | null;
  comics: IComics[];
}

export default function index({ detail }: { detail: ICharacterDetail }) {
  const dispatch = useDispatch();
  const bookmarks = useSelector((state: State) => state.bookmarks.characters);
  const [isBookmark, setBookmark] = useState(false);

  useEffect(() => {
    bookmarks.forEach((bookmark) => {
      if (bookmark.id === detail.id) setBookmark(true);
    });
  }, [bookmarks]);

  const clickRemove = () => {
    dispatch(removeBookmark('characters', detail.id));
    setBookmark(false);
  };

  return (
    <div className='detail-container'>
      <Link to='/characters' className='detail-button-back'>
        ⬅ Back
      </Link>
      <h1 className='detail-title'>{detail && detail.name}</h1>
      <img className='detail-img' src={detail && detail.img} alt='detail' />
      {detail && detail.description && <p className='detail-description'>{detail.description}</p>}
      {detail && detail.stories && <h3 className='detail-header'>Stories</h3>}
      {detail &&
        detail.stories &&
        detail.stories.map((story) => {
          const storyId = story.resourceURI.split('/');
          return (
            <div className='detail-info-item'>
              <Link className='detail-info-link' to={`/stories/${storyId[storyId.length - 1]}`}>
                {story.name}
              </Link>
            </div>
          );
        })}
      {detail && detail.comics && <h3 className='detail-header'>Comics</h3>}
      {detail &&
        detail.comics &&
        detail.comics.map((comic) => {
          const comicId = comic.resourceURI.split('/');
          return (
            <div className='detail-info-item'>
              <Link className='detail-info-link' to={`/comics/${comicId[comicId.length - 1]}`}>
                {comic.name}
              </Link>
            </div>
          );
        })}
      {isBookmark ? (
        <button className='detail-button detail-button-remove' type='button' onClick={clickRemove}>
          Remove bookmark ❌
        </button>
      ) : (
        <button
          className='detail-button detail-button-add'
          type='button'
          onClick={() => dispatch(addBookmark('characters', detail))}
        >
          Add to bookmarks 🧡
        </button>
      )}
      <button className='detail-button detail-button-source' type='button'>
        <a className='detail-button-source-content' href={detail && detail.source}>
          {' '}
          Go to source 🔼{' '}
        </a>
      </button>
    </div>
  );
}
