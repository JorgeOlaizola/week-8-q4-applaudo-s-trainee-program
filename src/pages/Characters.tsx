import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchCharacters } from '../API/fetchItems';
import CharactersContainer from '../components/Card/CardContainer';
import Pagination from '../components/Pagination/Pagination';
import Filters from '../components/Filters';
import TCard from '../interfaces/card/TCard';
import { dispatchCharacters } from '../state/actions/charactersActions';
import { State } from '../state/reducers';
import '../styles/cardPages.scss';

export default function Characters() {
  const characters = useSelector((state: State) => state.characters.characters);
  const [currentPage, setCurrentPage] = useState(1);
  const charactersPerPage = 10;
  const dispatch = useDispatch();
  const indexOfLastCharacter = currentPage * charactersPerPage;
  const indexOfFirstCharacter = indexOfLastCharacter - charactersPerPage;
  const currentCharacters: TCard[] = characters.slice(
    indexOfFirstCharacter,
    indexOfLastCharacter
  );

  useEffect(() => {
    const fetchFunction = async () => {
      const charactersResponse = await fetchCharacters();
      dispatch(dispatchCharacters(charactersResponse));
    };
    fetchFunction();
  }, []);
  return (
    <div className='card-page-container'>
      <h1 className='card-page-title'>Characters</h1>
      <Filters type='characters' />
      <CharactersContainer items={currentCharacters} type='characters' />
      <Pagination
        itemsPerPage={charactersPerPage}
        totalItems={characters.length}
        noScroll={false}
        currentPage={currentPage}
        paginate={setCurrentPage}
      />
    </div>
  );
}
