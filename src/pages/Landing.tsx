import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/landing.scss';

export default function Landing() {
  return (
    <div className='landing-container'>
      <h1 className='landing-title'>Marvel App</h1>
      <img
        className='landing-image'
        alt='landing'
        src='https://th.bing.com/th/id/R.0cf403a34d34cecbd9e6bd91cf8e1c0b?rik=0PTgrOCN8tfzhw&pid=ImgRaw&r=0'
      />
      <p className='landing-content'>
        Marvel Comics is the brand name and primary imprint of Marvel Worldwide Inc., formerly
        Marvel Publishing, Inc. and Marvel Comics Group, a publisher of American comic books and
        related media. In 2009, The Walt Disney Company acquired Marvel Entertainment, Marvel
        Worldwide&apos;s parent company. Marvel was started in 1939 by Martin Goodman under a number
        of corporations and imprints but now known as Timely Comics, and by 1951 had generally
        become known as Atlas Comics. The Marvel era began in 1961, the year that the company
        launched The Fantastic Four and other superhero titles created by Stan Lee, Jack Kirby,
        Steve Ditko and many others. The Marvel brand, which had been used over the years, was
        solidified as the company&apos;s primary brand.
      </p>
      <Link className='landing-link' to='/home'>
        Home
      </Link>
    </div>
  );
}
