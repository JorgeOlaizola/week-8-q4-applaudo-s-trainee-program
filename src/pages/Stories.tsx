import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchStories } from '../API/fetchItems';
import Pagination from '../components/Pagination/Pagination';
import StoriesContainer from '../components/Stories/StoriesContainer';
import TStoryCard from '../interfaces/card/TStoryCard';
import { dispatchStories } from '../state/actions/storiesActions';
import Filters from '../components/Filters';
import { State } from '../state/reducers';
import '../styles/stories.scss';
import '../styles/cardPages.scss';

export default function Stories() {
  const stories = useSelector((state: State) => state.stories.stories);
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  const storiesPerPage = 5;
  const indexOfLastStory = currentPage * storiesPerPage;
  const indexOfFirstStory = indexOfLastStory - storiesPerPage;
  const currentStories: TStoryCard[] = stories.slice(indexOfFirstStory, indexOfLastStory);

  useEffect(() => {
    const fetchFunction = async () => {
      const storiesResponse = await fetchStories();
      dispatch(dispatchStories(storiesResponse));
    };
    fetchFunction();
  }, []);

  return (
    <div className='stories-page-container'>
      <h1 className='card-page-title'>Stories</h1>
      <Filters type='stories' />
      <StoriesContainer stories={currentStories} />
      <Pagination
        itemsPerPage={storiesPerPage}
        totalItems={stories.length}
        noScroll={false}
        currentPage={currentPage}
        paginate={setCurrentPage}
      />
    </div>
  );
}
