import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../state/reducers';
import Pagination from '../components/Pagination/Pagination';
import CardContainer from '../components/Card/CardContainer';
import { dispatchComics } from '../state/actions/comicsActions';
import { fetchComics } from '../API/fetchItems';
import TCard from '../interfaces/card/TCard';
import Filters from '../components/Filters';
import '../styles/cardPages.scss';

export default function Comics() {
  const comics = useSelector((state: State) => state.comics.comics);
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  const comicsPerPage = 10;
  const indexOfLastComic = currentPage * comicsPerPage;
  const indexOfFirstComic = indexOfLastComic - comicsPerPage;
  const currentCharacters: TCard[] = comics.slice(indexOfFirstComic, indexOfLastComic);

  useEffect(() => {
    const fetchFunction = async () => {
      const comicsResponse = await fetchComics();
      dispatch(dispatchComics(comicsResponse));
    };
    fetchFunction();
  }, []);

  return (
    <div className='card-page-container'>
      <h1 className='card-page-title'>Comics</h1>
      <Filters type='comics' />
      <CardContainer items={currentCharacters} type='comics' />
      <Pagination
        itemsPerPage={comicsPerPage}
        totalItems={comics.length}
        noScroll={false}
        currentPage={currentPage}
        paginate={setCurrentPage}
      />
    </div>
  );
}
