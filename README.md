# Videogames App - Week 8 Q4 Applaudo Trainee Program

Application coded in TypeScript that uses the Marvel API (https://developer.marvel.com/documentation/) to fetch information. Created to learn 'global state' handling.

## Try it

Link to deploy: https://week-8-q4-applaudo-s-trainee-program.vercel.app/

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/JorgeOlaizola/week-8-q4-applaudo-s-trainee-program.git
```

Once the project it´s cloned, install dependencies

```bash
  npm install
```

Execute the following command to run the project in http://localhost:3000

```bash
  npm start
```
